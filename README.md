# Exercise 3: Generative Models

### **Submission -> January 23, 12:30 pm**

>**Administrative Note** Please create a separate repository for each exercise.
You should store the repositories within a gitlab group for your team.
This makes the organization alot easier for us.
You only need to invite us to your gitlab group once in order to give us access to your submissions.
Gitlab allows to transfer your existing repositories to gitlab groups under _Settings/General/Advanced/Transfer project_.

>Follow the instructions under [submissions](https://git.hhu.de/2022-ws-2023-unsuperviseddeeplearning/submissions) to create your submission.

>There are instructions on how to run your code on the HPC systems in the [solutions project](https://git.hhu.de/2022-ws-2023-unsuperviseddeeplearning/solutions/-/blob/main/HPC.md). We will talk about this in our exercises on _December 19_.

## Learning goals

- Handling 3D Polarized Light Imaging data in a deep learning pipeline
- Training of a [conditional] generative model of your choice
- Generate some pretty images!
- Evaluate the generated images by some evaluation metrics


## 3D Polarized Light Imaging

**Will be discussed on December 19**

3D Polarized Light Imaging (3D-PLI) [1](https://www.frontiersin.org/articles/10.3389/fninf.2011.00034/full) is a microscopic imaging technique to visualize nerve fibers and their 3D orientations in brain tissue sections. Nerve fiber connections between individual neurons allow communication and buildup the so called "connectome". PLI utilizes intrinsic physical properties of the tissue by measuring various optical effects, such as birefringence and light attenuation to visualize them. It stores the measured physical properties in different maps of the same section. These maps are also called _modalities_.

The **Transmittance** modality highlights the attenuation of light that passes the tissue section. It therefore depicts its shadow, which is influenced by the nerve fibers, but also cells, blood vessels and more. The maps are stored as grey-scale images:

<img src="media/transmittance.png" width="320" class="center"/>


The **Fiber Orientation Map (FOM)** modality highlights the 3D orientation (in-plane and out-of-plane) of nerve fibers at each pixel. It uses the [__HSV-colorspace__](https://de.wikipedia.org/wiki/HSV-Farbraum) to encode in-plane directions by the **h**ue and out-of-plane inclinations by the **v**alue. The maps are stored as RGB color images:

<img src="media/fom.png" width="320" class="center"/>



## Data Exploration

**Will be discussed on December 19**

Under [this link](https://fz-juelich.sciebo.de/s/Aqasuc4Vki567YX) you can find two folders `transmittance` and `fom`. Each folder consists of the same sections of two vervet monkey brains. While _Vervet1818_ was sectioned in the coronal plane (from the front to the back), _Vervet1947_ was sectioned in the saggital plane (from one side to the other).

Each section consists of one large file (up to 6.2 GB) in HDF5 format. Checkout [h5py](https://docs.h5py.org/en/stable/quick.html) for more info (if not done yet).
Each section file has the following structure:
```
Vervet([0-9]{4})_s([0-9]{4})_.*\.h5
├── Image
│   
└── pyramid
    ├── 00 -> /Image
    ├── 01
    ├── 02
    ...
    └── 10

```
The HDF5 dataset `Image` contains the full resolution section (1.3 µm) of shape (height, width, channels) for the FOM and (height, width) for the Transmittance maps as unsigned 8 bit integers from 0...255.
The HDF5 group `pyramid` consists of datasets `00` ... `10`. Each of these datasets is a downsampled version of the original image by a factor of 2 to the power of the dataset name. As dataset `00` would store the original image in its full resolution, it is only a link to the original `Image` dataset. In this way, we can visualize the data at different zoom levels without loading the complete section.

__Tasks__

1. Download a few sections of the _Vervet1818_ and at least one of the _Vervet1947_ sections. You will use _Vervet1818_ for training and _Vervet1947_ for evaluation.
2. Visualize an appropriate pyramid level for each of the sections to get an impression of the section content.
3. Locate some interesting structures and visualize full resolution crops of them (Don't load the full section to your memory).


### Sampling Strategy

**Will be discussed on December 19**

We will first have a look at the _FOM_ modality of the _Vervet1818_.
Since all of the sections probably won't fit into your RAM, we need to sample training images from disk.

__Tasks__

1. Adjust your __Dataset__ from exercise 2 such that it takes a list of section paths and stores an open [h5py File](https://docs.h5py.org/en/stable/high/file.html) handle for each.
Adjust the `__getitem__()` method such that it takes a tuple of `(brain, modality, section, row, column, tile_size)` as input and extracts a tile of size `(channels, tile_size, tile_size)` from the full resolution `Image` dataset from section `section` with modality `modality` from brain `brain` at location `(row, column)`.
Convert the tile to a `torch.Tensor` and normalize it to the range `[0, 1]` or `[-1, 1]`.
2. Adjust your __Sampler__ from exercise 2 such that it fits the new data scheme.
Overwrite the `__iter__()` method such that it yields random tuples of `(brain, modality, section, row, column, tile_size)`.
Note that we fix `brain=Vervet1818` and `modality=FOM` in the first model.

__Important__ As the _FOM_ modality consists of in-plane directions, encoded in the color, any data augmentation that changes the orientation of the images (flip, transpose, affine, rotation, ...) would require a careful re-coloring and resampling of the images to preserve the orientation information. For simplicity, you are not required to perform any data augmentation. There is enough data!

__Tip__ Since HDDs don't like random access patterns so much (in fact, they hate it!), make sure to load the data from an SSD device.

__Tip__ If training on the HPC systems, you can request nodes with enough memory to store all of the sections. You can give your __DataLoader__ a flag that loads the sections to memory or not depending on your system.

__Tip__ Make sure to keep the file handles open, as opening and closing induces a considerable overhead. Older versions of [h5py](https://docs.h5py.org/en/stable/quick.html) have bugs here. Make sure to install the latest version.


## Generative Models

**Will be discussed on January 9**

Implement a generative model of your choice for the _FOM_ maps of the _Vervet1818_.
Example for generative models include:
- [Generative Adversarial Networks (GAN)](https://dl.acm.org/doi/abs/10.1145/3422622)
- [Variational Autoencoder (VAE)](https://arxiv.org/abs/1312.6114)
- [Flow-based generative models](https://papers.nips.cc/paper/8224-glow-generative-flow-with-invertible-1x1-convolutions.pdf)
- [Diffusion Models](https://arxiv.org/abs/1503.03585)
- ...

While you are free in your choice, we suggest starting with a GAN, as they produce pretty images without too much computational ressources.

__Tasks__

1. Train your model on the _Vervet1818_ _FOM_ maps.
2. Generate some example images.

__Tip__ Use _sigmoid_ or _tanh_ activation for the output of your model to make sure the values stay within the range `[0, 1]` or `[-1, 1]`


## Conditional Generative Models

**Will be discussed on January 9**

3D-PLI provides different pixel-aligned views (modalities) for one and the same section. As examples, you have learned about the **Transmittance** and **FOM** modalities in the introduction.
An interesting question is to what extent the information of one modality is sufficient to generate the other.
In this task, you will adjust your generative model of the _FOM_ maps to be conditioned on the corresponding _Transmittance_ maps to address this question.

Examples for conditional generative models are:
- [Conditional Generative Adversarial Nets](https://arxiv.org/pdf/1411.1784.pdf)
- [Image-to-Image Translation with Conditional Adversarial Networks](https://openaccess.thecvf.com/content_cvpr_2017/html/Isola_Image-To-Image_Translation_With_CVPR_2017_paper.html)
- ...

__Tasks__

1. Adjust your **Dataset** to accept a sampling location without `modality`, such as `(brain, section, row, column, tile_size)` and adjust the `__getitem__()` function to return a tuple (or dictionary) of two PLI images. One is the `transmittance` and the other the corresponding `FOM` modality.
2. Train the conditional generative model of your choice to produce a `FOM` image given a `transmittance` image.


## Evaluation

**Will be discussed on January 16**

Evaluating generative models is often a difficult task and depends on the use case of the model. Therefore, there is no single metric to rule them all. In this task you will select a few of them and discuss their pros and cons.

Example metrics:
- Computation of log-likelihood of test samples
- [Inception Score (IS)](https://arxiv.org/abs/1606.03498)
- [Fréchet inception distance (FID)](https://papers.nips.cc/paper/2017/hash/8a1d694707eb0fefe65871369074926d-Abstract.html)
- Maximum Mean Discrepancy (MMD)
- Kernel Inception Distance (KID)
- Human assessments like [Human eYe Perceptual Evaluation: HYPE](https://proceedings.neurips.cc/paper/2019/hash/65699726a3c601b9f31bf04019c8593c-Abstract.html) (Your flatmate will surely be happy to assist you :D)
- Image statistic such as distances between histograms of colors or image gradients
- ...

__Tasks__

1. Decide for at least one evaluation metric to evaluate your generative model of `FOM` images. Perform an evaluation with this metric using the left out _Vervet1947_ section[s]. Discuss the pros and cons of your evaluation metric in terms of how it rates sample quality and diversity.
2. As we have paired images for the conditional generative model, we have access to groundtruth. Use the paired `transmittance` and `FOM` data for the _Vervet1947_ for the evaluation. Decide for an evaluation metric that takes advantage of this by comparing e.g. the pixel values or image statistics between the generated `FOM` and the original one.

__Tip__ If you have decided for an explicit density model with a tractable density, you can directly use the log-likelihood as evaluation metric. To apply it to an approximate density model such as an VAE you can still estimate it by performing importance sampling.


## Submitting Trained Models

Please include the final trained models in your submission.
Since adding large binary files (e.g., trained models) to `git`, we would as you to use [git lfs](https://git-lfs.github.com/) to include the models.
After installing `git-lfs` on your computer, use the following steps to configure `git-lfs` to track the binary files:

```bash
# Assuming your stored models end with .pkl

# Install git-lfs in your repository
git lfs install

# Ask git to track *.pkl files in git
git lfs track "*.pkl"

# Add configuration file and your models
git add .gitattributes *.pkl

# Commit and push
git commit -m "Added models"
git push
```

Please try to avoid extremely large numbers of models, since `git-lfs` still requires storage on the repository server.
Choose reasonable checkpoint intervals, or only submit the final model.

Finally, if you want us to run your trained model, please make it easy to load and apply.
You could provide a appropriately named script for this (e.g., `inference.py`), which creates the model, loads the trained weights, and runs an example image.

## Dataset Reference
Axer, M., Gräßel, D., Palomero-Gallagher, N., Takemura, H., Jorgensen, M. J., Woods, R., & Amunts, K. (2020). Images of the nerve fiber architecture at micrometer-resolution in the vervet monkey visual system [Data set]. EBRAINS. https://doi.org/10.25493/AFR3-KDK