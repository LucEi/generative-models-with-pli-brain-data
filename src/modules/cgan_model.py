from torch import nn

class Unflatten(nn.Module):
    def __init__(self, shape):
        super().__init__()
        self.shape = shape
    def forward(self, tensor):
        return tensor.view(*self.shape)

class ConvPoolDownsample(nn.Module):
    def __init__(self, in_c, out_c) -> None:
        super().__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(in_c, out_c, kernel_size=3, stride=1, padding=1),
            nn.LeakyReLU(0.2, inplace=True),
            nn.AvgPool2d(2, 2),
        )
    def forward(self, x):
        return self.layers(x)

class ConvNormDownsample(nn.Module):
    def __init__(self, in_c, out_c) -> None:
        super().__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(in_c, out_c, kernel_size=4, stride=2, padding=1),
            nn.BatchNorm2d(out_c),
            nn.LeakyReLU(0.2, inplace=True),
        )
    def forward(self, x):
        return self.layers(x)

class ConvNormUpsample(nn.Module):
    def __init__(self, in_c, out_c) -> None:
        super().__init__()
        self.layers = nn.Sequential(
            nn.ConvTranspose2d(in_c, out_c, kernel_size=4, stride=2,
                               padding=1, bias=False),
            nn.BatchNorm2d(out_c),
            nn.ReLU(inplace=True),
        )
    def forward(self, x):
        return self.layers(x)

class ConvDropSame(nn.Module):
    def __init__(self, in_c, out_c) -> None:
        super().__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(in_c, out_c, kernel_size=3, stride=1, padding=1),
            nn.Dropout2d(p=0.5),
            nn.LeakyReLU(0.2, inplace=True),
        )
    def forward(self, x):
        return self.layers(x)

class Discriminator(nn.Module):
    def __init__(self, features=64):
        super().__init__()
        self.layers = nn.Sequential(
            # NCHW = (bs, 4, ts, ts)
            # assuming stack of FOM + trans tiles = 3 + 1 channels
            ConvNormDownsample(4, features),     # 1/2
            ConvNormDownsample(features, features*2),     # 1/2
            ConvNormDownsample(features*2, features*4),     # 1/2
            ConvNormDownsample(features*4, features*4),     # 1/2
            nn.Conv2d(features*4, 1, kernel_size=4, stride=2, padding=1),    # 1/2
            nn.Linear(2, 1)
        )
    def forward(self, x):
        return self.layers(x)
        
class Generator(nn.Module):
    def __init__(self, latent_dim=1, features=64):
        super().__init__()
        self.layers = nn.Sequential(
            # NCHW = (bs, latent, ts, ts)
            ConvDropSame(latent_dim, features*8),   # same
            ConvDropSame(features*8, features*4),    # same
            ConvDropSame(features*4, features*2),  # same
            ConvDropSame(features*2, features),  # same
            nn.ConvTranspose2d(features, 3, kernel_size=3, stride=1,
                               padding=1, bias=False),  # same
            nn.Tanh(),
        )
    def forward(self, x):
        return self.layers(x)
