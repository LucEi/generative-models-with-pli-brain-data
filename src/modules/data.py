import pandas as pd
from .utils import randint, get_brain_mod_sec, mask_sampling
import h5py

import torch
from torchvision.transforms import Normalize
from torch.utils.data import Dataset, Sampler

class BrainDataset(Dataset):
    def __init__(self, paths, device='cpu') -> None:
        super().__init__()
        self.device = device
        self.transform = Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        self.data = {}
        for section_path in paths:
            self.data[get_brain_mod_sec(section_path)] = h5py.File(
                section_path)['pyramid']['00']

    def __getitem__(self, index):
        brain, modality, section, row, column, tile_size = index
        # avoid out of bound slicing, specially with images like section 850
        s = self.data[(brain, modality, section)].shape
        if s[0] - row <= 0:
            row = s[0] - tile_size
        if s[1] - column <= 0:
            column = s[1] - tile_size

        tile = self.data[(brain, modality, section)][row:row+tile_size, column:column+tile_size]
        tile = torch.tensor(tile, dtype=torch.float32, device=self.device)/255

        # outputs
        if modality == 'fom':
            return self.transform(tile.permute(2,0,1))

        elif modality == 'transmittance':
            return self.transform(tile.reshape(1, *tile.shape))

class BrainDatasetCGAN(Dataset):
    def __init__(self, fom_paths, trans_paths, device='cpu') -> None:
        super().__init__()
        self.device = device
        self.fom_transform = Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        self.trans_transform = Normalize(0.5, 0.5)
        self.fom_data = {}
        self.trans_data = {}
        for fom_path in fom_paths:
            self.fom_data[get_brain_mod_sec(fom_path)] = h5py.File(
                fom_path)['pyramid']['00']
        for section_path in trans_paths:
            self.trans_data[get_brain_mod_sec(section_path)] = h5py.File(
                section_path)['pyramid']['00']

    def __getitem__(self, index):
        brain, section, row, column, tile_size = index
        # avoid out of bound slicing, specially with images like section 850
        s = self.fom_data[(brain, 'fom', section)].shape
        if s[0] - row <= 0:
            row = s[0] - tile_size
        if s[1] - column <= 0:
            column = s[1] - tile_size

        fom_tile = self.fom_data[(brain, 'fom', section)
                                 ][row:row+tile_size, column:column+tile_size]
        fom_tile = torch.tensor(
            fom_tile, dtype=torch.float32, device=self.device)/255
        trans_tile = self.trans_data[(brain, 'transmittance', section)
                                     ][row:row+tile_size, column:column+tile_size]
        trans_tile = torch.tensor(
            trans_tile, dtype=torch.float32, device=self.device)/255

        return (self.fom_transform(fom_tile.permute(2,0,1)),
                self.trans_transform(trans_tile.reshape(1, *trans_tile.shape)))

class BrainSampler(Sampler):
    def __init__(self, paths, tile_size, epoch_length, mask_resolution='04', cgan=False, sample_sections=None):
        super().__init__(paths)
        self.mask_resolution = mask_resolution
        self.tile_size = tile_size
        self.epoch_length = epoch_length
        self.cgan = cgan
        brains = []
        modalities = []
        sections = []
        shapes = []
        masks = []
        for section_path in paths:
            brain, modality, section = get_brain_mod_sec(section_path)
            if sample_sections is None or section in sample_sections:
                with h5py.File(section_path) as f:
                    shape = f['pyramid']['00'].shape
                    # create mask for pixels that are not black
                    mask = (f['pyramid'][mask_resolution][:, :, 0:3] != [0,0,0]).all(-1)

                    shapes.append(shape)
                    masks.append(mask)
                    brains.append(brain)
                    modalities.append(modality)
                    sections.append(section)

        self.metadata = pd.DataFrame({'brain': brains, 'modality': modalities,
                                      'section': sections, 'img_shape': shapes, 'mask': masks})

    def __iter__(self):
        random_state = 13
        locs = self.metadata.sample(n=self.epoch_length, replace=True, random_state=random_state)
        for _, loc in locs.iterrows():
            shape = loc.img_shape
            r, c = mask_sampling(loc['mask'], self.tile_size, self.mask_resolution)
            if self.cgan:
                yield (loc.brain, loc.section, r, c, self.tile_size)
            else:
                yield (loc.brain, loc.modality, loc.section, r, c, self.tile_size)

    def __len__(self):
        return self.epoch_length
