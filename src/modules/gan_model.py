from torch import nn
class Unflatten(nn.Module):
    def __init__(self, shape):
        super().__init__()
        self.shape = shape
    def forward(self, tensor):
        return tensor.view(*self.shape)

class ConvPoolDownsample(nn.Module):
    def __init__(self, in_c, out_c) -> None:
        super().__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(in_c, out_c, kernel_size=3, stride=1, padding=1),
            nn.LeakyReLU(0.2, inplace=True),
            nn.AvgPool2d(2, 2),
        )
    def forward(self, x):
        return self.layers(x)

class ConvNormDownsample(nn.Module):
    def __init__(self, in_c, out_c) -> None:
        super().__init__()
        self.layers = nn.Sequential(
            nn.Conv2d(in_c, out_c, kernel_size=4, stride=2, padding=1),
            nn.BatchNorm2d(out_c),
            nn.LeakyReLU(0.2, inplace=True),
        )
    def forward(self, x):
        return self.layers(x)

class ConvNormUpsample(nn.Module):
    def __init__(self, in_c, out_c) -> None:
        super().__init__()
        self.layers = nn.Sequential(
            nn.ConvTranspose2d(in_c, out_c, kernel_size=4, stride=2,
                               padding=1, bias=False),
            nn.BatchNorm2d(out_c),
            nn.ReLU(inplace=True),
        )
    def forward(self, x):
        return self.layers(x)

class Discriminator(nn.Module):
    def __init__(self, features=64):
        super().__init__()
        self.layers = nn.Sequential(
            # NCHW = (bs, 3, ts, ts)
            ConvNormDownsample(3, features),     # 1/2
            ConvNormDownsample(features, features*2),     # 1/2
            ConvNormDownsample(features*2, features*4),     # 1/2
            ConvNormDownsample(features*4, features*4),     # 1/2
            nn.Conv2d(features*4, 1, kernel_size=4, stride=2, padding=1),    # 1/2
            nn.Linear(2, 1)
        )
    def forward(self, x):
        return self.layers(x)
        
class Generator(nn.Module):
    def __init__(self, latent_dim=128, features=64):
        super().__init__()
        self.layers = nn.Sequential(
            # NCHW = (bs, latent, 2, 2)
            ConvNormUpsample(latent_dim, features*4),   # x2
            ConvNormUpsample(features*4, features*4),    # x2
            ConvNormUpsample(features*4, features*2),  # x2
            ConvNormUpsample(features*2, features),  # x2
            nn.ConvTranspose2d(features, 3, kernel_size=4, stride=2,
                               padding=1, bias=False),  # x2
            nn.Tanh(),
        )
    def forward(self, x):
        return self.layers(x)
    