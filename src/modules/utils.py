import h5py
import re
import torch
import numpy as np
import matplotlib.pyplot as plt
from torchvision.utils import make_grid


def randint(high): return torch.randint(high, (1,))


def get_brain_mod_sec(section_path):
    brain = re.search('Vervet\d{4}', section_path).group()
    if 'FOM' in section_path:
        modality = 'fom'
    elif 'Transmittance' in section_path:
        modality = 'transmittance'
    section = re.search("s\d{4}(_left|_right|_cerebellum|_FOM_HSV)", section_path).group()
    return brain, modality, section

def mask_sampling(mask, tile_size, mask_resolution):
    scale_factor = 2**int(mask_resolution)
    # sample until pixel corresponds to actual foreground (brain in our case)
    foreground = False
    while not foreground:
        mask_r = randint(mask.shape[0] - (tile_size//scale_factor+1) )
        mask_c = randint(mask.shape[1] - (tile_size//scale_factor+1) )
        foreground = mask[mask_r, mask_c]

    # scale back coordinates
    return scale_factor * mask_r, scale_factor * mask_c

def show_image(file, resolution, hzoom=None, wzoom=None, axis=False):
    '''shows a brain image either in original size (with percentual scale)
    or cropped image (with pixel scale) in the given resolution'''
    image = h5py.File(file)['pyramid'][resolution]
    brain = re.search('Vervet\d{4}', file).group()
    section = re.search("s\d{4}", file).group()
    print(f'{image.shape} = original image shape')
    hextent, wextent = image.shape[:2]
    hslice = slice(*np.array(hzoom)*hextent//100) if hzoom else slice(None,None)
    wslice = slice(*np.array(wzoom)*wextent//100) if wzoom else slice(None,None)

    htick_locs = np.linspace(0,hextent,11)
    wtick_locs = np.linspace(0,wextent,11)

    zoom_img = image[hslice,wslice]
    print(f'{zoom_img.shape} = zoomed image shape')
    plt.imshow(zoom_img, cmap='gray')
    if hzoom or wzoom:
        plt.xticks(ticks=[0, zoom_img.shape[1]],
                   labels=np.array(wzoom)*wextent//100)
        plt.yticks(ticks=[0, zoom_img.shape[0]],
                   labels=np.array(hzoom)*hextent//100)
    else:
        plt.xticks(ticks=wtick_locs, labels=range(0,110,10))
        plt.yticks(ticks=htick_locs, labels=range(0,110,10))
    plt.title('brain %s, section %s' % (brain, section))
    plt.axis(axis)
    plt.show();


def show_batch(images, save=None):
    with torch.no_grad():
        img_grid = make_grid(images, normalize=True)#, scale_each=True)
        plt.figure(figsize=(12,8))
        plt.imshow(img_grid.permute(1, 2, 0))
        plt.axis(False)
        plt.savefig(save) if save else plt.show()

def sample_noise(batch_size, noise_dim):
    return torch.randn(batch_size, noise_dim, 2, 2)  # format NCHW
